﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewBehaviourScript : MonoBehaviour
{
    public RenderTexture _renderTexture;
    public MeshRenderer _target;

    [Header("Click me im actually a button when in Play")]
    public bool RUN = false;

    void Run()
    {
        // consider caching this in OnAwake or Start, quite heavy
        // we need to create an active render texture with correct dimensions, otherwise you'll get some wonky texture-in-texture stuff
        RenderTexture rendy = new RenderTexture(_renderTexture.width, _renderTexture.height, 24, RenderTextureFormat.R16);
        RenderTexture.active = rendy;

        // active render texture is going to be copied to this
        Texture2D texxy = new Texture2D(_renderTexture.width, _renderTexture.height, TextureFormat.R16, 0, true);

        // copy depth render texture to active render texture we just created
        Graphics.Blit(_renderTexture, RenderTexture.active);

        // render a "blank" texture on our tex2d to know if we fuck up, (you will get a black / green striped texture when copying fails)
        for (int index = 0; index < _renderTexture.height; index++)
        {
            for (int index2 = 0; index2 < _renderTexture.width; index2++)
            {
                if (index % 2 == 1)
                    texxy.SetPixel(index2, index, Color.red);

                else
                    texxy.SetPixel(index2, index, Color.green);
            }
        }
        //

        // read pixels from active render texture into our texture
        texxy.ReadPixels(new Rect(0, 0, _renderTexture.width, _renderTexture.height), 0, 0, false);
        texxy.Apply();

        // apply tex on some quad to see results visually
        _target.material.mainTexture = texxy;

        // to avoid bugs on other scripts using render textures
        RenderTexture.active = null;

    }

    private void OnValidate()
    {
        if (RUN)
        {
            RUN = false;
            Run();
        }
    }

}
